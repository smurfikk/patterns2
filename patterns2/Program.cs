﻿using System;
using System.Collections.Generic;

public interface IObserver
{
    void Update(IMessage message);
}

public interface IMessage
{
    string Sender { get; set; }
    string GetContent();
}

public class User : IObserver
{
    private string name;

    public User(string name)
    {
        this.name = name;
    }

    public void Update(IMessage message)
    {
        Console.WriteLine($"{name} received: {message.GetContent()}");
    }

    public void SendMessage(IServer server, string receiver, IMessageBuilder builder)
    {
        builder.SetSender(name);
        server.SendMessage(receiver, builder);
    }
}

public interface IServer
{
    void Register(User user, string name);
    void SendMessage(string receiver, IMessageBuilder builder);
}

public class RealServer : IServer
{
    private Dictionary<string, IObserver> users = new Dictionary<string, IObserver>();

    public void Register(User user, string name)
    {
        users[name] = user;
    }

    public void SendMessage(string receiver, IMessageBuilder builder)
    {
        var msg = builder.GetMessage();

        if (users.ContainsKey(receiver))
        {
            users[receiver].Update(msg);
        }
    }
}

public class ServerProxy : IServer
{
    private RealServer server;

    public ServerProxy(RealServer server)
    {
        this.server = server;
    }

    public void SendMessage(string receiver, IMessageBuilder builder)
    {
        server.SendMessage(receiver, builder);
    }

    public void Register(User user, string name)
    {
        server.Register(user, name);
    }
}

public interface IMessageBuilder
{
    void SetSender(string sender);
    IMessage GetMessage();
}

public class TextMessageBuilder : IMessageBuilder
{
    private TextMessage message = new TextMessage();

    public TextMessageBuilder(string content)
    {
        message.Content = content;
    }

    public void SetSender(string sender)
    {
        message.Sender = sender;
    }

    public IMessage GetMessage()
    {
        return message;
    }
}

public class VoiceMessageBuilder : IMessageBuilder
{
    private VoiceMessage message = new VoiceMessage();

    public VoiceMessageBuilder(string content)
    {
        message.Content = "Voice Message: ~" + content + "~";
    }

    public void SetSender(string sender)
    {
        message.Sender = sender;
    }

    public IMessage GetMessage()
    {
        return message;
    }
}

public class TextMessage : IMessage
{
    public string Sender { get; set; }
    public string Content { get; set; }

    public string GetContent()
    {
        return $"{Sender}: {Content}";
    }
}

public class VoiceMessage : IMessage
{
    public string Sender { get; set; }
    public string Content { get; set; }

    public string GetContent()
    {
        return $"{Sender}: {Content}";
    }
}


class Program
{
    static void Main(string[] args)
    {
        RealServer realServer = new RealServer();
        IServer server = new ServerProxy(realServer);

        User john = new User("John");
        User alice = new User("Alice");

        server.Register(john, "John");
        server.Register(alice, "Alice");

        john.SendMessage(server, "Alice", new TextMessageBuilder("Hello, Alice!"));
        alice.SendMessage(server, "John", new VoiceMessageBuilder("Hello"));


    }

}
